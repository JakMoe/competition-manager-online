﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competition_manager_online.Models
{
    public class CompetitorDbContext : DbContext
    {

        public CompetitorDbContext(DbContextOptions<CompetitorDbContext> options) : base(options) { }

        public DbSet<Competitor> Competitors { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Competitor>().ToTable("Competitor");
        }



    }
}
