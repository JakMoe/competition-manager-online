﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Competition_manager_online.Models
{
    public class Competitor
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public int Age { get; set; }
        public string Nationality { get; set; }
    }
}
